# Kick Assembler for VsCode

This is a Kick Assembler Extension for Visual Studio Code. It is intended to provide the following in-editor features:
* Source editing for the 6502 using the [Kick Assembler] (http://www.theweb.dk/KickAssembler/Main.html#frontpage)
* Compiling and Running Generated Programs (PRG)

#### **This Extension is a work-in-progress and that many features do not work yet.**

## Installation
The latest version of the Extension is available on the GitLab repository at:
https://gitlab.com/Retro65/kickass-vscode-ext/pipelines

## Requirements
You need to have Kick Assembler and Java already installed on your computer. Java does **not** need to be in your path.
If you want to run compiled programs, then you should also have WinVice or an equivalent emulator installed on your computer as well.

### Preferences

_Please note that if you are running on Windows, you must include `'\\'` for path names._

- **kickass.assemblerPath**
This is the full path to your 'kickass.jar' file.

`C:\\Path\\To\\kickass.jar`

- **kickass.javaPath**
This is the full path to your Java binary. If Java is in your system Path, you can probably just put 'java' or 'java.exe'

`C:\\Path\\To\\Java.exe`

- **kickass.emulatorPath**
The full path to your Emulator. If this is already in your system Path, you can probably just leave this blank.

`C:\\Path\\To\\WinVice`

- **kickass.emulatorBin**
The name of your emulator binary.

`x64.exe`

## Commands
Coming Soon

## Known Issues
Many

## Release Notes
Check the [changelog](CHANGES.md)

## Contributing
Bug reports, fixes, and other changes are welcome. The repository is [on GitLab](https://gitlab.com/Retro65/kickass-vscode-ext)

## How to Run Locally
* `npm install` to initialize the extension and the server
* `npm run compile` to compile the extension and the server
* open this folder in VS Code. In the Debug viewlet, run 'Launch Client' from drop-down to launch the extension and attach to the extension.
* to debug the server use the 'Attach to Server' launch config.
* set breakpoints in the client or the server

## Acknowledgements
This Extension is heavily **inspired** (see copied) from the wonderful work of Zeh Fernando and his [Dasm Macro Assembler for VSCode](https://github.com/zeh/vscode-dasm.git). I want to thank Zeh for his inspirational work, and wonderfully easy code base to *steal* -- ahem -- base this extension on.

I also want to acknowledge Thomas Conte who wrote the original extension for VSCode which allowed building from within the editor.

Last, I want to recognize Simon Oskarsson (Swoffa) for his awesome work on the [Sublime Text Extension](https://github.com/Swoffa/SublimeKickAssemblerC64/tree/master) which contains the wicked TM Language file that this extension uses. Thanks to him and other previous authors and contributors.

## License
This extension uses the [MIT license](https://en.wikipedia.org/wiki/MIT_License)

