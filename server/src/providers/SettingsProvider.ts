/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import * as fs from 'fs';

import {
	DidChangeConfigurationParams,
	IConnection,
} from "vscode-languageserver";

import { IProjectInfoProvider, Provider } from "./Provider"

interface IGlobalSettings {
	["kickassembler"]:ISettings;
}

export interface ISettings {
    assemblerPath: string,
    javaPath: string,
    emulatorPath: string,
    emulatorBin: string,
    preferUppercase:  UppercaseTypes[],
    valid: boolean
}

export type UppercaseTypes = "instructions"|"pseudoops"|"registers"|"all";

export default class SettingsProvider extends Provider {

    private _current:ISettings

    constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {
        log.trace("[SettingsProvider]");
        
        super(connection, projectInfoProvider);

        connection.onDidChangeConfiguration((changeConfigurationParams:DidChangeConfigurationParams) => {
            log.trace("[SettingsProvider] onDidChangeConfiguration");
            const settings = <IGlobalSettings>changeConfigurationParams.settings;
            this.process(<ISettings>settings["kickass"])
        })
    }

    public getCurrent():ISettings {
        return this._current;
    }

    private process(newSettings:ISettings) {
        newSettings.valid = true;
        //  check "assemblerPath"
        if (!fs.existsSync(newSettings.assemblerPath)) {
            newSettings.valid = false;
            log.error("assemblePath is invalid");
        }
        //  check "javaPath"
        if (!fs.existsSync(newSettings.javaPath)) {
            newSettings.valid = false;
            log.error("javaPath is invalid");
        }
        this._current = newSettings;
    }
}