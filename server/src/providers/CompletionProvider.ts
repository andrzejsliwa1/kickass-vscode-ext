/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import {
	CompletionItem,
	CompletionItemKind,
	IConnection,
	TextDocumentPositionParams,
	TextEdit
} from "vscode-languageserver";

import StringUtils from "../utils/StringUtils";
import LanguageDefinition, { IInstruction, IPseudoOp, IPreProcessor } from "../definitions/LanguageDefinition";
import { ISymbol } from "./Assembler";
import { IProjectInfoProvider, Provider } from "./Provider";
import { IAssemblerResults, IDirective, ISyntax, ILine, ISourceRange } from "../assembler/KickAssembler";
import { SymbolTypes } from "../project/Project";
import LineUtils from "../utils/LineUtils";

enum LanguageCompletionTypes {
	Instruction,
	Symbol,
	Label,
	Register,
	PseudoOp,
	PreProcessor,
	Directives,
}

interface ICompletionData {
	type: LanguageCompletionTypes;
	payload: Object;
}

export default class CompletionProvider extends Provider {

	private documentPosition:TextDocumentPositionParams;
	private documentSource:string[];

	private trigger:string;
	private triggerToken:string;
	private triggerLine:string;

	private intelligentLabels:boolean;


	constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {

        log.trace("[CompletionProvider]");
		super(connection, projectInfoProvider);
		
		// 	show the initial list of items
		connection.onCompletion((textDocumentPosition:TextDocumentPositionParams): CompletionItem[] => {
            log.trace("[CompletionProvider] onCompletion");
			this.documentPosition = textDocumentPosition;
			return this.createCompletionItems(textDocumentPosition);
		});

		//	request for more details of a particular item
		connection.onCompletionResolve((item:CompletionItem):CompletionItem => {
            log.trace("[CompletionProvider] onCompletionResolve");
			return this.processItem(item);
		});
	}

	private createCompletionItems(textDocumentPosition:TextDocumentPositionParams):CompletionItem[] {

		/*
			working out some new pseudo code for completion logic

			no tokens?
				trigger is #?
					show pre-processor directives
				trigger is "."
					show directives
			there are tokens?
				last token ended with ";", ":", "{", "}"?
					trigger is "."?
						show directives
				last token was an instruction or pseudocommand?
					show vars, labels, macros, functions
				last token was nothing?
					show instructions, macros, pseudocommands

		*/

		log.trace("[CompletionProvider] createCompletionItems");

		if (this.getProjectInfo().getAssemblerResults().status > 0) {
			return [ {
				label: "ERROR"
			}];
		}

		const lineNumber = textDocumentPosition.position.line;
		const line:ILine = this.getProjectInfo().getLines()[lineNumber];
		const items:CompletionItem[] = [];
		
		const settings = this.getProjectInfo().getSettings();
		const assemblerResults:IAssemblerResults = this.getProjectInfo().getAssemblerResults();

		var loaded:boolean;

		var trim = require('trim');

		this.documentSource = this.getProjectInfo().getSource();
		this.triggerLine = trim.left(this.documentSource[textDocumentPosition.position.line]);
		this.trigger = "";

		//	find position of trigger if available

		//	is it from a #?
		var pos = this.triggerLine.indexOf("#");

		//	is it from a .?
		if (pos < 0) {
			pos = this.triggerLine.indexOf(".");
		}

		//	trigger character found as first character on line
		if (pos == 0) {
			this.trigger = this.triggerLine.substr(pos,1);
		} 

		// 	preproc only on "#" trigger
		if (this.trigger == "#" && !loaded ) {
			for (let pp of assemblerResults.assemblerInfo.preprocessorDirectives) {
				items.push(this.createCompletionItem(pp.name.toLocaleLowerCase(), LanguageCompletionTypes.PreProcessor, pp, CompletionItemKind.Interface, textDocumentPosition));
				loaded = true;
			}
		} 

		//	directives only on '.' trigger with no token
		if (this.trigger == "." && !loaded ) {
			for (let directive of assemblerResults.assemblerInfo.directives) {
				items.push(this.createCompletionItem(directive.name.toLocaleLowerCase(), LanguageCompletionTypes.Directives, directive, CompletionItemKind.Interface, textDocumentPosition));
				loaded = true;
			}
		}

		log.debug("loaded = " + loaded);

		if (!loaded) {

			var prevToken:string = "";

			//	get tokens
			var tokens = this.triggerLine.split(" ");
			log.debug("tokens = " + tokens);

			//	do we have some tokens?
			if (tokens.length > 1) {
				//	grab the previous token
				var token = tokens[tokens.length - 2];
				//var token = LineUtils.getTokenAtPosition(this.triggerLine, textDocumentPosition.position.character);
				log.debug("token = " + token);
				//	look for a matching instruction
				const instructionMatch = LanguageDefinition.Instructions.find((instruction) => {
					return instruction.name.toLowerCase() === token.toLowerCase();
				});
				//	if there is a match, flag it as instruction
				if (instructionMatch) {
					prevToken = "instruction";
				}
			}

			log.debug("prevToken = " + prevToken);

			// 	show instructions, macros and pseudocommands
			if (prevToken == "") {

				//for (let directive of assemblerResults.assemblerInfo.directives) {
				//	items.push(this.createCompletionItem(directive.name.toLocaleLowerCase(), LanguageCompletionTypes.Directives, directive, CompletionItemKind.Interface, textDocumentPosition));
				//	loaded = true;
				//}

				for (let instruction of LanguageDefinition.Instructions) {
					const name = instruction.name.toLocaleLowerCase();
					items.push(this.createCompletionItem(name, LanguageCompletionTypes.Instruction, instruction, CompletionItemKind.Text, textDocumentPosition));
					loaded = true;
				}

				//	insert macros
				this.loadSymbols(line, textDocumentPosition, SymbolTypes.Macro, items);

				//	insert pseudocommands
				this.loadSymbols(line, textDocumentPosition, SymbolTypes.PseudoCommand, items);
			}	

			if (prevToken == "instruction") {
				log.debug(this.getProjectInfo().getSymbols());
				this.loadSymbols(line, textDocumentPosition, SymbolTypes.Function, items);
				this.loadSymbols(line, textDocumentPosition, SymbolTypes.Variable, items);
				this.loadSymbols(line, textDocumentPosition, SymbolTypes.Label, items);
			}
		}
		return items;
	}

	private loadSymbols(line:ILine, textDocumentPosition:TextDocumentPositionParams, symbolType:SymbolTypes, items:CompletionItem[]) {
		if (this.getProjectInfo().getSymbols()) {
			for(let symbol of this.getProjectInfo().getSymbols()) {
				//	see if we already have a variable by that name
				if (symbol.type == symbolType) {
					const symbolMatch = items.find((the_symbol) => {
						return the_symbol.label.toLowerCase() === symbol.name.toLowerCase();
					});
					//	if there is no match
					if (!symbolMatch) {
						//	make sure it is in scope
						if (symbol.scope <= line.scope) {
							items.push(this.createCompletionItem(symbol.name, LanguageCompletionTypes.Directives, symbol, symbol.kind, textDocumentPosition));
						}
					}
				}
			}
		}
	}	

	private processItem(item:CompletionItem):CompletionItem {
		item.detail = item.data.payload.detail;
		return item;
	}

	private createCompletionItem(label:string, type:LanguageCompletionTypes, payload:any, kind:CompletionItemKind, textPosition:TextDocumentPositionParams):CompletionItem {
		
		let filterText = label;
		let insertText = label;

		if (this.trigger.length > 0 ) {
			filterText = filterText.substr(1,filterText.length-1);
			insertText = label.substr(1, label.length);
		}

		return {
			label,
			kind,
			filterText: filterText,
			insertText: insertText,
			data: {
				type,
				payload,
				},
		};
	
		//return { label, kind };
	}

}
