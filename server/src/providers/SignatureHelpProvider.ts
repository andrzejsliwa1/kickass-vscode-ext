/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import { IConnection, SignatureHelp, TextDocumentPositionParams, SignatureInformation, ParameterInformation,
} from "vscode-languageserver";
import { IProjectInfoProvider, Provider } from "./Provider";

export default class SignatureHelpProvider extends Provider {

    private _textDocumentPositionParams:TextDocumentPositionParams;

	constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {
		log.trace('[SignatureHelpProvider]');
        super(connection, projectInfoProvider);
        
        connection.onSignatureHelp((textDocumentPositionParams:TextDocumentPositionParams):SignatureHelp => {
            log.trace('[SignatureHelpProvider] onSignatureHelp');
            this._textDocumentPositionParams = textDocumentPositionParams;
            return this.process();
        });
        connection
	}

	public process():SignatureHelp {
        log.trace('[SignatureHelpProvider] process');

		if (this.getProjectInfo().getAssemblerResults().status > 0) {
            return null;
		}

        if (!this.getProjectInfo()) return;

        let lines = this.getProjectInfo().getLines();
        let line = lines[this._textDocumentPositionParams.position.line];
        let char = line.text.substr(this._textDocumentPositionParams.position.character-1,1);
        if (char == ")") return null;
        
        let signatureHelp:SignatureHelp;
        let signatureInformation:SignatureInformation;
        let parameterInformation1:ParameterInformation;
        let parameterInformation2:ParameterInformation;
        let parameterInformation3:ParameterInformation;
        
        parameterInformation1 = {
            label: "param1",
            documentation: "param doc1"
        }

        parameterInformation2 = {
            label: "param2",
            documentation: "param doc2"
        }

        parameterInformation3 = {
            label: "param3",
            documentation: "param doc3"
        }

        signatureInformation = {
            label: "hello",
            documentation: "hello world",
            parameters: [parameterInformation1, parameterInformation2, parameterInformation3]

        }

        signatureHelp = {
            signatures: [signatureInformation],
            activeSignature: 0,
            activeParameter: 0
        }

        return signatureHelp;
	}

}
