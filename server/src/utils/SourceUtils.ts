/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/


import { ILine, ISyntax, ISourceRange } from "../assembler/KickAssembler";
import LineUtils from "./LineUtils";

export default class SourceUtils {

	public static getLines(sourcelines:string[]):ILine[]|undefined {
		var lines = [];
		var next = 0;
		
		/**
		 * something
		 */
		var scope = 0;	//	something
		var last = []
		for(var i = 0; i < sourcelines.length; i++) {

			var source = LineUtils.removeComments(sourcelines[i]); 

			if (source) {

				//	search for { - add to scope
				if (source.indexOf("{") >= 0) {
					next += 1;
					last.push(scope);
					scope = next;
				}
				//	search for } - remove from scope
				if (source.indexOf("}") >= 0) {
					scope = last.pop();
				}
			}

			//	add new line info object
			var newline = <ILine> {};
			newline.number = i+1;
			newline.scope = scope;
			newline.text = sourcelines[i];
			lines.push(newline);
		}
		return lines;
    }
    
    //  create a symbol from source
    public static createSymbol(syntax:ISyntax, range:ISourceRange, lines:ILine[]) {
        
    }
    
        
}