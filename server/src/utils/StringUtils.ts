/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

export default class StringUtils {

	public static splitIntoLines(text:string):string[] {
		return text.split(/\r?\n/g);
	}

	public static removeWrappingQuotes(text:string):string {
		if (text) {
			const start = text.substr(0, 1);
			const end = text.substr(text.length - 1);
			if (start === end && start === "\"" || start === "'") {
				return text.substr(1, text.length - 2);
			}
		}
		return text;
	}

/**
 * alksjdlaksjd
 * @param text 
 */

 	private x:string;

	public static splitFunction(text:string):string[]|undefined {
		let vals:string[] = [];
		//	remove parenthesis
		text = text.trim();
		text = text.replace("(", " ");
		text = text.replace(")", " ");
		text = text.replace("{", " ");
		text = text.replace("/", " ");

		//	split by blanks to get function name and parms
		let v1 = text.split(" ");
		vals.push(v1[0].trim());
		
		//	split parms by comma
		if (v1[1]) {
			let parms = v1[1].split(",");
			parms.forEach(element => {
				vals.push(element.trim());
			});
		}
		return vals;
	}

	public static splitPseudoCommand(text:string):string[]|undefined {
		let vals:string[] = [];
		//	remove parenthesis
		text = text.replace("(", " ");
		text = text.replace(")", " ");
		text = text.replace("{", " ");
		text = text.replace("/", " ");

		//	split by blanks to get function name and parms
		let v1 = text.split(" ");
		vals.push(v1[0].trim());
		
		//	split parms by comma
		if (v1[1]) {
			let parms = v1[1].split(":");
			parms.forEach(element => {
				vals.push(element.trim());
			});
		}
		return vals;
	}

	public static joinStringArray(text:string[]):string {
		var ret = "";
		for (var i = 0; i < text.length; i++) {
			ret += text[i] + "\n";
		}
		return ret;
	}

	public static removeCRLF(text:string):string {
		return text.replace(/(\r\n|\n|\r)/gm,"");
	}
}
