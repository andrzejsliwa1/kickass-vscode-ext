/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/


'use strict';

import * as path from 'path';
import * as vscode from 'vscode';

import { workspace, Disposable, ExtensionContext, commands, window } from 'vscode';
import { LanguageClient, LanguageClientOptions, SettingMonitor, ServerOptions, TransportKind } from 'vscode-languageclient';
import { CommandRun } from './commands/CommandRun';
import { CommandBuild } from './commands/CommandBuild';
import ConfigUtils from './utils/ConfigUtils';


export function activate(context: ExtensionContext) {

	//	the server is implemented in node
	let serverModule = context.asAbsolutePath(path.join('server', 'server.js'));
	// 	the debug options for the server
	let debugOptions = { execArgv: ["--nolazy", "--inspect=6009"] };
	console.log(debugOptions);
	
	// If the extension is launched in debug mode then the debug server options are used
	// Otherwise the run options are used
	let serverOptions: ServerOptions = {
		run : { module: serverModule, transport: TransportKind.ipc },
		debug: { module: serverModule, transport: TransportKind.ipc, options: debugOptions }
	}
	
	// Options to control the language client
	let clientOptions: LanguageClientOptions = {
		// Register the server for plain text documents
		documentSelector: ['kickass'],
		synchronize: {
			// Synchronize the setting section 'languageServerExample' to the server
			configurationSection: 'kickass',
			// Notify the server about file changes to '.clientrc files contain in the workspace
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	}

	context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(configChanged));

	// Create the language client and start the client.
	let languageServer = new LanguageClient('kickass', 'Kick Assembler Language Extension Client', serverOptions, clientOptions);
	let disposable = languageServer.start();

	// Push the disposable to the context's subscriptions so that the 
	// client can be deactivated on extension deactivation
	context.subscriptions.push(disposable);

	//	wait for the language server to be ready
    languageServer.onReady().then(() => {

		//	catch notifications from the language server
		languageServer.onNotification( "ERROR", (message:string) => {
			vscode.window.showErrorMessage(message);
		});

	});
		
	//	create command for assembling
	let cmdBuild = commands.registerCommand("kickass.build", function() {
		commandBuild(context);
	});

	//	create command to run
	let cmdRun = commands.registerCommand("kickass.run", function() {
		commandRun(context);
	});

	//	create command to build & run
	let cmdBuildRun = commands.registerCommand("kickass.buildandrun", function() {
		commandBuildRun(context);
	});

	context.subscriptions.push(cmdBuild);
	context.subscriptions.push(cmdRun);
	context.subscriptions.push(cmdBuildRun);
	
	ConfigUtils.validateSettings();

	console.log("kickass-vscode-ext extension is active.");
}

export function deactivate() {
	console.log("kickass-vscode-ext extension has been deactivated.");
}

function commandBuild(context:ExtensionContext):number {
	console.log("[ClientExtension] commandCompile");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Build - Check Settings");
		return;
	}
	var cb = new CommandBuild(context);
	return cb.build();
}

function commandRun(context:ExtensionContext) {
	console.log("[ClientExtension] commandRun");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Run - Check Settings");
		return;
	}
	var cr = new CommandRun(context);
	cr.run()
}

function commandBuildRun(context:ExtensionContext) {
	console.log("[ClientExtension] commandCompileRun");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Build and Run - Check Settings");
		return;
	}
    window.activeTextEditor.document.save().then( function(reponse) {
		if (commandBuild(context) == 0) {
			commandRun(context);
		}
	});
}

function configChanged(params) {
	ConfigUtils.validateSettings();
}