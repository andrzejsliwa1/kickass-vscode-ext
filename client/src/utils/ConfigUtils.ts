import * as vscode from 'vscode';
import * as fs from 'fs';

export default class ConfigUtils {

	public static validateSettings():boolean {
		let settings = vscode.workspace.getConfiguration("kickass");

		if (!fs.existsSync(settings.get("assemblerPath"))) {
			vscode.window.showErrorMessage("Invalid Assembler Path");
			return false;
		}
		
		if (!fs.existsSync(settings.get("javaPath"))) {
			vscode.window.showErrorMessage("Invalid Java Path");
			return false;
		}

        return true;
	}
}