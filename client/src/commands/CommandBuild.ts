/*
	Copyright (C) SpockerDotNet LLC. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/


import { spawn, spawnSync } from 'child_process';
import { workspace, window, Disposable, ExtensionContext, commands, WorkspaceConfiguration } from 'vscode';
import PathUtils from '../utils/PathUtils';  

export class CommandBuild { 

    private _configuration;

    constructor(context:ExtensionContext) {
        this._configuration = workspace.getConfiguration('kickass');
    }


    public build():number {

        //  is the java path set?
        let javaPath = this._configuration.get("javaPath");

        //  is the kickass path set?
        let kickPath = this._configuration.get("assemblerPath");

        //  locate file, does it exist?
        let doc = window.activeTextEditor.document;
        let file = PathUtils.uriToFileSystemPath(doc.uri.toString());
        console.log(`- looking for file ${file}`);

        //  create new output channel
        let outputChannel = window.createOutputChannel('Kick Assembler Build');
        outputChannel.clear();
        outputChannel.show();

        //  spawn new child process
        let java = spawnSync(javaPath, ["-jar", kickPath, file]);

        let errorCode = java.status;

        if (errorCode > 0) {
            outputChannel.append(java.stderr.toString());
            window.showErrorMessage('Compilation failed with errors.');
        } else {
            outputChannel.append(java.stdout.toString());
        }

        return errorCode;
    }

    
}

