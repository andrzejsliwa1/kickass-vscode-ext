# Changelog

All notable changes to the kickass-vscode-ext are listed here.

## 0.9.5
### Added
- include file support
- scope support (experimental)
- source verification delay (experimental)
- macros, functions and pseudocommands from include files
- invalid settings feedback for remediation
### Changed
- definitions sourced from assembler
- processor directives sourced from assembler
- upgrade to newest language server protocol 
- symbol support completely redone to make other providers easier to implement
- added hover information for numeric/hex/binary/decimal values
- update to gitlab build script to accomodate new project structure
### Fixes
- #2 - incorrect comment block marker
- #1 - errors on completion provider
### Issues
- signature help *might* occur on macros and functions
- hover thinks EVERYTHING is a symbol

## 0.9.4 (2017-07-15)
### Added
- emulator runtime options
- compile program (assemble)
- compile & run (F5)
- run emulator on current project
- document save on compile & run
### Issues
- only supports running PRG files currently

## 0.9.2 (2017-07-05)
### Added
- limited symbol scoping (breaks some things too)
### Changed
- support for posix (OSX & Linux) and Win32 paths
### Minor
- changed GitLab building when Tagged only

## 0.9.0 (2017-07-01)
### Added
- syntax highlighting
- inline error detection
- basic symbol/label hover support
- autocomplete for instructions, symbols and labels
### Minor
- autobuild on GitLab



## Legend
- **Added**     - new additions to the extension
- **Changed**   - modification to existing features
- **Removed**   - feature removed
- **Minor**     - minor update
- **Fixes**     - fixes in this release
- **Issues**    - known issues in this release